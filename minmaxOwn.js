let myboard = ["x", "o", "", "x", "o", "x", "", "", ""];
const WIN_COMB = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
]

function isGameOver(gameboard, player) {
    return WIN_COMB.some(row => row.every(index => gameboard[index] == player));
}

function isGameDraw(gameboard) {
    return WIN_COMB.every(row => row.every(index => gameboard[index] != ""));
}

console.log("move" + bestmove(myboard, 0));

function bestmove(myboard, depth) {
    let bests = -100;
    let move;
    for (var i = 0; i < myboard.length; i++) {
        if (myboard[i] == "") {
            myboard[i] = "o";
            let m = minmax(myboard, true, depth);
            console.log("final output " + myboard + " id" + m + " i" + i);
            myboard[i] = "";
            if (m > bests) {
                bests = m;
                move = i;
            }
        }
    }
    return move;
}
//minmax(myboard, false, 0);

function minmax(board, ismax, depth) {
    if (isGameOver(board, "x")) {
        return 100 - depth;
    } else if (isGameOver(board, "o")) {
        return depth - 100;
    } else if (isGameDraw(board)) {
        return 0;
    } else {
        if (ismax) {
            let bestm = -100;
            for (var i = 0; i < board.length; i++) {
                if (board[i] == "") {
                    let newboard = board;
                    newboard[i] = "x";
                    let m = minmax(newboard, false, depth + 1);
                    bestm = Math.max(m, bestm);
                    newboard[i] = "";
                    //console.log("X turn " + newboard + "best " + Math.max(m, bestm) + "m" + m);
                }
            }
            return bestm;
        } else {
            let bestm = 100;
            for (var i = 0; i < board.length; i++) {
                if (board[i] == "") {
                    let newboard = board;
                    newboard[i] = "o";
                    let m = minmax(newboard, true, depth + 1);
                    bestm = Math.min(m, bestm);
                    //console.log("0 turn " + newboard + " best" + Math.min(m, bestm) + "m" + m);
                    newboard[i] = "";
                }
            }
            return bestm;
        }
    }
}