const X_PLAYER = 'X';
const Y_PLAYER = 'O';
const gameoverboard = document.getElementById("gameovermodal");
const winner = document.getElementById("winner");
let playerTurn = true;
let player1TurnCount = document.getElementById("pscore1");
let player2TurnCount = document.getElementById("pscore2");
let boardIndex = document.querySelectorAll("td");
let boardvalues = new Array(3);
boardvalues[0] = [0, 1, 2];
boardvalues[1] = [3, 4, 6];
boardvalues[2] = [6, 7, 7];
const WIN_COMB = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
]

init();

function init() {
    document.getElementById("restartbut").addEventListener("click", drawBoard);
    for (var i = 0; i < boardIndex.length; i++) {
        boardIndex[i].addEventListener("click", markOnBoard);
    }

}

function drawBoard() {
    boardIndex.forEach(index => index.innerText = "");
    init();
}

function markOnBoard(event) {
    let id = event.target.id;
    let player = playerTurn ? X_PLAYER : Y_PLAYER;
    nextPlayer(id, X_PLAYER);
    if (isGameOver(player)) {
        showGameOver(
            `Player ${player} wins the game`);
        console.log("Winner => " + player);
    } else if (isGameDraw()) {
        showGameOver(`Game Draw`);
        console.log("draw");
    } else {
        /* let minmaxboard = updateBoard();
        let nextmove = bestMove(minmaxboard, 0);
        nextPlayer(boardvalues[nextmove[0]]
            [nextmove[1]], Y_PLAYER); */

    }
}

function updateBoard() {
    let minmaxboard = new Array(3);
    minmaxboard[0] = new Array(3);
    minmaxboard[1] = new Array(3);
    minmaxboard[2] = new Array(3);
    for (var i = 0; i < boardIndex.length; i++) {
        minmaxboard[Math.floor(i / 3)][Math.floor(i % 3)] = boardIndex[i].innerHTML;
    }
    return minmaxboard;
}

function showGameOver(message) {
    winner.innerText = message;
    $('#gameovermodal').modal({
        show: true,
        backdrop: "static"
    });
}

function swapPlayer() {
    playerTurn = !playerTurn;
}

function nextPlayer(id, player) {
    if (player == X_PLAYER) {
        document.getElementById(id).classList = "PX";
    } else {
        document.getElementById(id).classList = "PO";
    }
    document.getElementById(id).innerHTML = player;
    document.getElementById(id).removeEventListener("click", markOnBoard);
}

function isGameOver(player) {
    return WIN_COMB.some(row => row.every(index => boardIndex[index].innerHTML == player));
}

function isGameDraw() {
    return WIN_COMB.every(row => row.every(index => boardIndex[index].innerHTML != ""));
}