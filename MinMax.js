function checkWinner(board) {
    let X_win = WIN_COMB.some(row => row.every(index => board[Math.floor(index / 3)][Math.floor(index % 3)] == "X"));
    let O_win = WIN_COMB.some(row => row.every(index => board[Math.floor(index / 3)][Math.floor(index % 3)] == "0"));
    let gamedraw = WIN_COMB.every(row =>
        row.every(index => board[Math.floor(index / 3)][Math.floor(index % 3)] != ""));
    if (X_win) {
        return "xwin";
    } else if (O_win) {
        return "owin";
    } else if (gamedraw) {
        return "draw";
    }
}

function minmax2(minmaxboard, depth, isMaximizing) {
    var win = checkWinner(minmaxboard);
    if (win == "xwin") {
        return (10 - depth);
    } else if (win == "owin") {
        return (depth - 10);
    } else if (win == "draw") {
        return 0;
    } else {
        if (isMaximizing) {
            let bestscore = -Infinity;
            for (var i = 0; i < 3; i++) {
                for (var j = 0; j < 3; j++) {
                    if (minmaxboard[i][j] == "") {
                        minmaxboard[i][j] = "X";
                        let score = minmax2(minmaxboard, depth + 1, false);
                        minmaxboard[i][j] = "";
                        bestscore = Math.max(score, bestscore);
                    }
                }
            }
            return bestscore;
        } else {
            let bestscore = -Infinity;
            for (var i = 0; i < 3; i++) {
                for (var j = 0; j < 3; j++) {
                    if (minmaxboard[i][j] == "") {
                        minmaxboard[i][j] = "O";
                        let score = minmax2(minmaxboard, depth + 1, true);
                        minmaxboard[i][j] = "";
                        bestscore = Math.min(score, bestscore);
                    }
                }
            }
            return bestscore
        }
    }
}

function bestMove(minmaxboard, depth) {
    let bestscore = -Infinity;
    let move;
    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            if (minmaxboard[i][j] == "") {
                minmaxboard[i][j] = "O";
                let score = minmax2(minmaxboard, depth, true);
                minmaxboard[i][j] = '';
                if (score > bestscore) {
                    bestscore = score;
                    move = [i, j];
                }
            }
        }
    }
    console.log("Bestscore" + bestscore);
    console.log("output move" + move);
    return move;
}